This is a heavily-modified fork of the [pcap4j](https://github.com/kaitoy/pcap4j) library.
That library is focused on friendly parsing of packet data (among other things). This
library is simply focused on speed. As such, every attempt is made to avoid object
creation and to keep the tight loops small and efficient.

Sample usage:

    :::java
    public class Loop {
        public static void main( String[] args ) throws Exception {
            String ifc_name = null;

            // Use passed-in name, if applicable
            if ( args.length > 0 ) {
                ifc_name = args[ 0 ];
            }

            PcapNetworkInterface ifc;

            // If a name wasn't passed in, determine which to use. This will prompt the
            // user via the console.
            if ( ifc_name == null ) {
                ifc = new NifSelector().selectNetworkInterface();
            }
            // If a name was provided, use the NIC with that name.
            else {
                ifc = Pcaps.getNifByName( ifc_name );
            }

            final PcapHandle handle =
                ifc.openLive( 65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS,
                10 /* ms */ );

            handle.fastLoop( null, new PacketListener() {
                public void gotPacket( byte[] data, int captureLength, int length,
                    long secs, long usecs ) {

                    System.out.println( "Captured " + captureLength + " bytes" );
                }
            } );
        }
    }

Special thanks to [Kaito Yamada](https://github.com/kaitoy) for his work on the original
pcap4j and making it available.