package com.logicartisan.pcap.sample;

import com.logicartisan.pcap.core.PacketListener;
import com.logicartisan.pcap.core.PcapHandle;
import com.logicartisan.pcap.core.PcapNetworkInterface;
import com.logicartisan.pcap.core.Pcaps;
import com.logicartisan.pcap.util.NifSelector;

import java.io.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 *
 */
public class CaptureToPcap {
	public static void main( String[] args ) throws Exception {
		File file = null;
		Integer seconds = null;
		String ifc_name = null;
		int snaplen = 0xFFFF;
		for( int i = 0; i < args.length; i++ ) {
			if ( file != null ) {
				printUsage();
			}

			String arg = args[ i ];

			switch( arg ) {
				case "-t":
					seconds = new Integer( args[ i + 1 ] );
					i++;
					break;

				case "-i":
					ifc_name = args[ i + 1 ];
					i++;
					break;

				case "-l":
					snaplen = Integer.parseInt( args[ i + 1 ] );
					i++;
					if ( snaplen > 0xFFFF ) {
						System.err.println( "Invalid capture size" );
						System.exit( -1 );
					}
					break;

				default:
					file = new File( arg );
					break;
			}
		}

		if ( file == null ) {
			printUsage();
		}
		else {
			System.out.println( "Writing to file: " + file.getAbsolutePath() );
		}

		PcapNetworkInterface ifc;
		if ( ifc_name == null ) {
			ifc = new NifSelector().selectNetworkInterface();
		}
		else {
			ifc = Pcaps.getNifByName( ifc_name );
		}

		boolean abend = true;
		FileOutputStream fout = null;
		BufferedOutputStream bout = null;
		DataOutputStream out = null;
		try {
			fout = new FileOutputStream( file, false );
			bout = new BufferedOutputStream( fout, 4096 * 1024 );
			out = new DataOutputStream( bout );

			writeHeader( out, snaplen );
			abend = false;
		}
		finally {
			if ( abend ) {
				close( out );
				close( bout );
				close( fout );

				file.delete();
			}
		}

		final PcapHandle handle = ifc.openLive( snaplen,
			PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, 10 /* ms */ );

		final CountDownLatch close_latch = new CountDownLatch( 1 );

		if ( seconds != null ) {
			final long milliseconds = TimeUnit.SECONDS.toMillis( seconds.intValue() );
			new Thread( "Timer" ) {
				@Override
				public void run() {
					try {
						Thread.sleep( milliseconds );
					}
					catch( InterruptedException ex ) {
						// fall through
					}
					handle.breakLoop();
				}
			}.start();
		}

		// Add a shutdown hook to cancel the capture so it's flushed properly.
		Runtime.getRuntime().addShutdownHook( new Thread() {
			@Override
			public void run() {
				handle.breakLoop();

				try {
					close_latch.await( 5, TimeUnit.SECONDS );
				}
				catch ( InterruptedException e ) {
					// ignore
				}
			}
		} );

		final DataOutput final_out = out;
		try {
			handle.fastLoop( null, new PacketListener() {
				@Override
				public void gotPacket( byte[] data, int captureLength, int length, long secs,
					long usecs ) {

					System.out.println( "Packet: " + captureLength );
					try {
						final_out.writeInt( ( int ) secs );
						final_out.writeInt( ( int ) usecs );
						final_out.writeInt( captureLength );
						final_out.writeInt( length );
						final_out.write( data, 0, captureLength );
					}
					catch ( IOException ex ) {
						System.err.println( "Error writing data: " + ex );
						System.exit( -1 );
					}
				}
			} );
		}
		catch( InterruptedException ex ) {
			// ignore
		}
		finally {
			System.out.println( "nice close");
			close( out );
			close( bout );
			close( fout );

			close_latch.countDown();
		}

		System.exit( 0 );
	}


	private static void writeHeader( DataOutput out, int snaplen ) throws IOException {
		out.writeInt( 0xa1b2c3d4 );     // magic number
		out.writeShort( 2 );            // major version
		out.writeShort( 4 );            // minor version
		out.writeInt( 0 );              // timezone offset
		out.writeInt( 0 );              // sigfigs
		out.writeInt( snaplen );        // snaplen
		out.writeInt( 1 );              // data link type
	}


	private static void close( Closeable c ) {
		if ( c == null ) return;
		try {
			c.close();
		}
		catch( IOException ex ) {
			// ignore
		}
	}


	private static void printUsage() {
		System.out.println( "CaptureToPcap [-t <time>] [-i <ifc>] [-l <length>] <file>" );
		System.out.println( "  Capture packets and write data to a .pcap file" );
		System.out.println();
		System.out.println( "Options:" );
		System.out.println( "  [-t   <time>]   Time (in seconds) for which to capture data" );
		System.out.println( "  [-i    <ifc>]   Name of the capture interface" );
		System.out.println( "  [-l <length>]   Max capture length" );
		System.exit( 0 );
	}
}
