package com.logicartisan.pcap.sample;

import com.logicartisan.pcap.core.*;
import com.logicartisan.pcap.util.NifSelector;

import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicLong;


/**
 *
 */
public class Loop {
	public static void main( String[] args ) throws Exception {
		String ifc_name = null;

		// Use passed-in name, if applicable
		if ( args.length > 0 ) {
			ifc_name = args[ 0 ];
		}

		PcapNetworkInterface ifc;

		// If a name wasn't passed in, determine which to use. This will prompt the
		// user via the console.
		if ( ifc_name == null ) {
			ifc = new NifSelector().selectNetworkInterface();
		}
		// If a name was provided, use the NIC with that name.
		else {
			ifc = Pcaps.getNifByName( ifc_name );
		}

		final PcapHandle handle =
			ifc.openLive( 65536, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS,
			10 /* ms */ );

		final AtomicLong bytesCounter = new AtomicLong( 0 );
		final AtomicLong packetsCounter = new AtomicLong( 0 );

		// Thread that will print capture stats every 5 seconds.
		new Thread() {
			long last_time = System.currentTimeMillis();

			final NumberFormat formatter = NumberFormat.getNumberInstance();

			boolean printed_stats_error = false;

			@Override
			public void run() {
				while( true ) {
					try {
						Thread.sleep( 5000 );
					}
					catch ( InterruptedException e ) {
						return;
					}

					long time = System.currentTimeMillis();
					double seconds = ( time - last_time ) / 1000.0;
					last_time = time;

					long packets = packetsCounter.getAndSet( 0 );
					long bytes = bytesCounter.getAndSet( 0 );

					double pps = packets / seconds;
					double bps = bytes / seconds;

					System.out.println( packets + " packets (" +
						formatter.format( pps ) + " pps) @ " + formatter.format( bps ) +
						" bps" );

					try {
						PcapStats stats = handle.getStats();
						System.out.println( "   (recv: " + stats.getReceived() +
							"  drop: " + stats.getDropped() + "  drop@ifc: " +
							stats.getDroppedByInterface() + ")" );
					}
					catch( Exception ex ) {
						if ( !printed_stats_error ) {
							ex.printStackTrace();
							printed_stats_error = true;
						}
					}
				}
			}
		}.start();

		// Start the capture loop. This will block until complete (forever in this example
		// since we're not specifying a number of packets to capture and don't have
		// a thread calling breakLoop().
		handle.fastLoop( null, new PacketListener() {
			public void gotPacket( byte[] data, int captureLength, int length, long secs,
				long usecs ) {

				packetsCounter.incrementAndGet();
				bytesCounter.addAndGet( captureLength );
			}
		} );
	}
}
