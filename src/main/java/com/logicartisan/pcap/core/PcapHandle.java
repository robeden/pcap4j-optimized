/*_##########################################################################
  _##
  _##  Copyright (C) 2011-2012  Kaito Yamada
  _##
  _##########################################################################
*/

package com.logicartisan.pcap.core;

import com.logicartisan.pcap.core.NativeMappings.bpf_program;
import com.logicartisan.pcap.core.NativeMappings.pcap_pkthdr;
import com.logicartisan.pcap.util.ByteArrays;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * A wrapping class for struct pcap_t.
 * On some OSes, such as Linux and Solaris, this class can't capture packets
 * whose source and destination interface are the same.
 *
 * @author Kaito Yamada
 * @since pcap4j 0.9.1
 */
public final class PcapHandle {

	private static final Logger logger = LoggerFactory.getLogger( PcapHandle.class );

	private final int maxCaptureLength;
	private final DataLinkType dlt;
	private final Pointer handle;
	private final Object thisLock = new Object();

	private volatile boolean open;
	private volatile String filteringExpression = "";

	private static final Inet4Address WILDCARD_MASK;

	static {
		try {
			WILDCARD_MASK = ( Inet4Address ) InetAddress.getByName( "0.0.0.0" );
		}
		catch ( UnknownHostException e ) {
			throw new AssertionError( "never get here" );
		}
	}

	PcapHandle( Pointer handle, boolean open, int maxCaptureLength ) {
//    this.dlt = DataLinkType.getInstance(
//                 PcapLibrary.INSTANCE.pcap_datalink(handle)
//               );
		this.dlt = DataLinkType.getInstance(
			NativeMappings.pcap_datalink( handle )
		);
		this.handle = handle;
		this.open = open;
		this.maxCaptureLength = maxCaptureLength;
	}

	/**
	 * @return the Data Link Type of this PcapHandle
	 */
	public DataLinkType getDlt() { return dlt; }

	/**
	 * @return true if this PcapHandle is open; false otherwise.
	 */
	public boolean isOpen() { return open; }

	/**
	 * @return the filtering expression of this PcapHandle
	 */
	public String getFilteringExpression() {return filteringExpression; }

	/**
	 * @author Kaito Yamada
	 * @version pcap4j 0.9.1
	 */
	public static enum BpfCompileMode {
		OPTIMIZE( 1 ),
		NONOPTIMIZE( 0 );

		private final int value;

		private BpfCompileMode( int value ) {
			this.value = value;
		}

		/**
		 * @return value
		 */
		public int getValue() {
			return value;
		}
	}

	/**
	 *
	 * @param bpfExpression
	 * @param mode
	 * @param netmask
	 * @throws PcapNativeException
	 * @throws IllegalStateException
	 * @throws NullPointerException
	 */
	public void setFilter(
		String bpfExpression, BpfCompileMode mode, Inet4Address netmask
	) throws PcapNativeException {
		if (
			bpfExpression == null
				|| mode == null
				|| netmask == null
			) {
			StringBuilder sb = new StringBuilder();
			sb.append( "bpfExpression: " ).append( bpfExpression )
				.append( " mode: " ).append( mode )
				.append( " netmask: " ).append( netmask );
			throw new NullPointerException( sb.toString() );
		}

		synchronized ( thisLock ) {
			if ( !open ) {
				throw new IllegalStateException( "Not open." );
			}

			int mask = ByteArrays.getInt( ByteArrays.toByteArray( netmask ), 0 );

			bpf_program prog = new bpf_program();
			try {
//        int rc = PcapLibrary.INSTANCE.pcap_compile(
//                   handle, prog, bpfExpression, mode.getValue(), mask
//                 );
				int rc = NativeMappings.pcap_compile(
					handle, prog, bpfExpression, mode.getValue(), mask
				);
				if ( rc < 0 ) {
					throw new PcapNativeException(
						"Error occured in pcap_compile: " + getError()
					);
				}

				// rc = PcapLibrary.INSTANCE.pcap_setfilter(handle, prog);
				rc = NativeMappings.pcap_setfilter( handle, prog );
				if ( rc < 0 ) {
					throw new PcapNativeException(
						"Error occured in pcap_setfilger: " + getError()
					);
				}

				this.filteringExpression = bpfExpression;
			}
			finally {
				// PcapLibrary.INSTANCE.pcap_freecode(prog);
				NativeMappings.pcap_freecode( prog );
			}
		}
	}

	/**
	 *
	 * @param bpfExpression
	 * @param mode
	 * @throws PcapNativeException
	 * @throws IllegalStateException
	 * @throws NullPointerException
	 */
	public void setFilter(
		String bpfExpression, BpfCompileMode mode
	) throws PcapNativeException {
		setFilter( bpfExpression, mode, WILDCARD_MASK );
	}


	/**
	 * A wrapper method for "int pcap_loop(pcap_t *, int, pcap_handler, u_char *)".
	 * Once a packet is captured, listener.gotPacket(Packet) is called in the same thread,
	 * and this won't capture any other packets until the thread finishes.
	 */
	public void fastLoop( Integer packetCount, PacketListener listener )
		throws PcapNativeException, InterruptedException {

		int rc;

		byte[] data = new byte[ maxCaptureLength ];

		synchronized ( thisLock ) {
			if ( !open ) {
				throw new IllegalStateException( "Not open." );
			}

			logger.info( "Start loop" );
			rc = NativeMappings.pcap_loop(
				handle,
				packetCount == null ? 0 : packetCount.intValue(),
				new FastPacketFuncCallback( listener, data ),
				null
			);
		}

		switch ( rc ) {
			case 0:
				logger.info( "Finish loop." );
				break;
			case -1:
				throw new PcapNativeException( "Error occured: " + getError() );
			case -2:
				logger.info( "Broken." );
				throw new InterruptedException();
			default:
				throw new PcapNativeException( "Unexpected error occured: " +
					getError() );
		}
	}

	private final class FastPacketFuncCallback implements NativeMappings.pcap_handler {
		private final PacketListener listener;
		private final byte[] data;

		private FastPacketFuncCallback( PacketListener listener, byte[] data ) {
			this.listener = listener;
			this.data = data;
		}

		public void got_packet( Pointer args, pcap_pkthdr header,
			Pointer packet ) {

			final long sec = header.ts.tv_sec.longValue();
			final long usec = header.ts.tv_sec.longValue();

			final int caplen = header.caplen;
			packet.read( 0, data, 0, caplen );

			listener.gotPacket( data, caplen, header.len, sec, usec );
		}
	}

	/**
	 * Breaks a loop which this handle is working on.
	 * <p/>
	 * The loop may not be broken immediately on some OSes
	 * because of buffering or something.
	 * As a workaround, letting this capture some bogus packets
	 * after calling this method may work.
	 */
	public void breakLoop() {
		logger.info( "Break loop." );
		// PcapLibrary.INSTANCE.pcap_breakloop(handle);
		NativeMappings.pcap_breakloop( handle );
	}


	/**
	 * Returns stats for a capture session.
	 */
	public PcapStats getStats() throws PcapNativeException {
		NativeMappings.pcap_stats stats = new NativeMappings.pcap_stats();

		int rc = NativeMappings.pcap_stats( handle, stats );

		if ( rc != 0 ) {
			throw new PcapNativeException( "Unable to get stats" );
		}

		return new PcapStats( stats.ps_recv, stats.ps_drop, stats.ps_ifdrop );
	}


	/**
	 *
	 */
	public void close() {
		synchronized ( thisLock ) {
			if ( !open ) {
				logger.warn( "Already closed." );
				return;
			}
			// PcapLibrary.INSTANCE.pcap_close(handle);
			NativeMappings.pcap_close( handle );
			open = false;
		}

		logger.info( "Closed." );
	}

	/**
	 * @return an error message.
	 */
	public String getError() {
		// return PcapLibrary.INSTANCE.pcap_geterr(handle).getString(0);
		return NativeMappings.pcap_geterr( handle ).getString( 0 );
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder( 60 );

		sb.append( "Link type: [" ).append( dlt )
			.append( "] handle: [" ).append( handle )
			.append( "] Open: [" ).append( open )
			.append( "] Filtering Expression: [" ).append( filteringExpression )
			.append( "]" );

		return sb.toString();
	}

}
