package com.logicartisan.pcap.core;

/**
 * Simple container class for stats about a capture.
 *
 * @see PcapHandle#getStats
 */
public class PcapStats {
	private final int received;
	private final int dropped;
	private final int dropped_by_interface;

	public PcapStats( int received, int dropped, int dropped_by_interface ) {
		this.received = received;
		this.dropped = dropped;
		this.dropped_by_interface = dropped_by_interface;
	}

	public int getReceived() {
		return received;
	}

	public int getDropped() {
		return dropped;
	}

	public int getDroppedByInterface() {
		return dropped_by_interface;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "PcapStats{" );
		sb.append( "received=" ).append( received );
		sb.append( ", dropped=" ).append( dropped );
		sb.append( ", dropped_by_interface=" ).append( dropped_by_interface );
		sb.append( '}' );
		return sb.toString();
	}
}
