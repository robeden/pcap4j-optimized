package com.logicartisan.pcap.core;

/**
 *
 */
public interface PacketListener {
	/**
	 * Called when new packet data has arrived. The capture will be blocked while this
	 * method is executed, so it should return as quickly as possible.
	 *
	 * @param data          Reused (!) array containing packet data. The array is reused,
	 *                      so a reference to it MUST NOT be held outside this method.
	 *                      The array will likely be longer than the actual packet data.
	 *                      See <tt>captureLength</tt> for the actual data length.
	 * @param captureLength The amount of packet data captured and available in
	 *                      <tt>data</tt>.
	 * @param length        The real packet length on the wire. This may be longer than
	 *                      the capture length.
	 * @param secs          Seconds portion of the packet time.
	 * @param usecs         Microseconds portion of the packet time.
	 */
	public void gotPacket( byte[] data, int captureLength, int length, long secs,
		long usecs );
}
