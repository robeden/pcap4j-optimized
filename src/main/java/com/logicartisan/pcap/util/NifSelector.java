/*_##########################################################################
  _##
  _##  Copyright (C) 2012  Kaito Yamada
  _##
  _##########################################################################
*/

package com.logicartisan.pcap.util;

import com.logicartisan.pcap.core.PcapAddress;
import com.logicartisan.pcap.core.PcapNativeException;
import com.logicartisan.pcap.core.PcapNetworkInterface;
import com.logicartisan.pcap.core.Pcaps;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


/**
 * @author Kaito Yamada
 * @since pcap4j 0.9.1
 */
public class NifSelector {

	private static String LINE_SEPARATOR = System.getProperty( "line.separator" );

	/**
	 * @return a PcapNetworkInterface object which represents
	 *         a selected network interface.
	 */
	public
	final PcapNetworkInterface selectNetworkInterface()
		throws IOException {
		List<PcapNetworkInterface> allDevs = null;
		try {
			allDevs = Pcaps.findAllDevs();
		}
		catch ( PcapNativeException e ) {
			throw new IOException( e.getMessage() );
		}

		if ( allDevs == null || allDevs.size() == 0 ) {
			throw new IOException( "No NIF to capture." );
		}

		// If there's only one device, select it.
		if ( allDevs.size() == 1 ) return allDevs.get( 0 );

		showNifList( allDevs );

		return doSelect( allDevs );
	}

	/**
	 *
	 * @param msg
	 * @throws IOException
	 */
	protected void write( String msg ) throws IOException {
		System.out.print( msg );
	}

	/**
	 * @return string
	 */
	protected String read() throws IOException {
		BufferedReader reader
			= new BufferedReader( new InputStreamReader( System.in ) );
		return reader.readLine();
	}

	/**
	 *
	 * @param nifs
	 * @throws IOException
	 */
	protected void showNifList( List<PcapNetworkInterface> nifs )
		throws IOException {
		StringBuilder sb = new StringBuilder( 200 );
		int nifIdx = 0;
		for ( PcapNetworkInterface nif : nifs ) {
			sb.append( "NIF[" ).append( nifIdx ).append( "]: " )
				.append( nif.getName() ).append( LINE_SEPARATOR )
				.append( "      : description: " )
				.append( nif.getDescription() ).append( LINE_SEPARATOR );

			for ( PcapAddress addr : nif.getAddresses() ) {
				sb.append( "      : address: " )
					.append( addr.getAddress() ).append( LINE_SEPARATOR );
			}
			nifIdx++;
		}
		sb.append( LINE_SEPARATOR );

		write( sb.toString() );
	}

	/**
	 * @return a PcapNetworkInterface object which represents
	 *         a selected network interface.
	 */
	protected PcapNetworkInterface doSelect( List<PcapNetworkInterface> nifs )
		throws IOException {
		int nifIdx;
		while ( true ) {
			write( "Select a device number to capture packets, or enter 'q' to quit > " );

			String input;
			if ( ( input = read() ) == null ) {
				return null;
			}

			if ( input.equals( "q" ) ) {
				return null;
			}

			try {
				nifIdx = Integer.parseInt( input );
				if ( nifIdx < 0 || nifIdx >= nifs.size() ) {
					write( "Invalid input." + LINE_SEPARATOR );
					continue;
				}
				else {
					break;
				}
			}
			catch ( NumberFormatException e ) {
				write( "Invalid input." + LINE_SEPARATOR );
				continue;
			}
		}

		return nifs.get( nifIdx );
	}

}
